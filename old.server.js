const IS_DEV = process.platform === 'win32',
	devlog = (...arguments) => IS_DEV ? console.log.apply(console, arguments) : null

const config = require("./config.json")
	WebSocket = require('ws'),
	wss = new WebSocket.Server({ port: config.port }),

	Message = require("./src/Message"),
	Settings = require("./src/Settings"),

	settings = new Settings(),
	GPIOs = IS_DEV ? null : require("./src/Gpios"),
	{ sha256 } = require("js-sha256")

const authenticatedWebSocket = (ee) => {
	console.log("authenticated!")
	const authenticatedRequest = message => {
		console.log("got authentication message!", message.v)
		if (message.type === 'set-pin-value')
			GPIOs.setup(message.v.pinId, {
				mode: GPIOs.OUTPUT
			}).set(message.v.value)
	}

	ee.on('message', authenticatedRequest)
}

const checkpasswordWebSocket = function(ee) {
	console.log('mdr')
	let tries = 0

	const checkpasswordListener = message => {
		console.log(tries++, message.v.password)
		if (message.type === 'login')
			if (message.v.password !== sha256('password'))
				return message.reply({ result: false })
			else {
				ee.removeListener('message', checkpasswordListener)
				authenticatedWebSocket(message.reply({ result: true }, true))
			}
	}

	ee.on('message', checkpasswordListener)
}

module.exports = function() {
	if (IS_DEV)
		console.log('Mode développement activé.')

	wss.on('connection', ws => {
		devlog('Connexion.')

		ws.once('message', message_ => {
			try {
				let message = new Message(ws, message_)

				// WIP
				if (message.type === "hello")
					checkpasswordWebSocket(
						message.reply({ "name": "Le Raspberry", "configured": true, "passwordRequired": true }, true)
					)
				else
					ws.terminate()
			} catch(e) {

			}
		});
	
		ws.send('CONNECTED');
	});

	console.log('Serveur en écoute sur le port', config.port)
}
