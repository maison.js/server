const crypto = require('crypto'),
    { sha256 } = require("js-sha256")

module.exports = class Crypting {
    constructor() {
        this.serverKey = crypto.getDiffieHellman('modp14')
        this.aesKey = null
    }

    getPublicKey() {
        this.serverKey.generateKeys()
        
        return this.serverKey.getPublicKey()
    }

    computeSecret(bobsPublicKey) {
        this.aesKey = this.serverKey.computeSecret(Buffer.from(bobsPublicKey))
    }

    crypt(str) {
        let key = this.aesKey.slice(0, 256 / 8), // key length de 256/8 (nb de bytes) avec l'algo aes-256-cbc
            cipher = crypto.createCipheriv('aes-256-cbc', key, sha256(key).substring(0, 16)),
            crypted = cipher.update(str, 'utf8', 'hex')

        crypted += cipher.final('hex')
        return crypted
    }

    decrypt(str) {
        let key = this.aesKey.slice(0, 256 / 8),
            decipher = crypto.createDecipheriv('aes-256-cbc', key, sha256(key).substring(0, 16)),
            decrypted = decipher.update(str, 'hex', 'utf8')

        decrypted += decipher.final('utf8')
        return decrypted
    }
}