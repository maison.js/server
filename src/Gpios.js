const GPIO = require("pigpio").Gpio

let GPIOs = [], selectedGPIO = null

module.exports = class GPIOs {
	static get GPIOs() { return GPIOs }
	static get selectedGPIO() { return selectedGPIO }
	static set selectedGPIO(v) { selectedGPIO = v }

	static setup(pin, defaultOptions) {
		this.selectedGPIO = pin

		if (!GPIOs[pin])
			GPIOs[pin] = new GPIO(pin, defaultOptions)

		return this
	}

	static get _current() { return this.GPIOs[this.selectedGPIO] }

	/**
	 * 
	 * @param {number} value Entre 0 et 255
	 */
	static set(value) {
		this._current.pwmWrite(value)
	}
}

module.exports.OUTPUT = GPIO.OUTPUT
module.exports.INPUT = GPIO.INPUT
