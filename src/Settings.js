const fs = require("promisified-fs");

class Settings {
	constructor(file = "./user/settings.json") {
		this.file = file
		this.data = null

		// Optimisation de l'IO disque
		this.changed = false

		this.autosaveInterval = null
	}

	async read() {
		try {
			await fs.access(this.file, fs.constants.F_OK);
			
			let content = await fs.readFile(this.file, "utf8");
			this.data = JSON.parse(content);
		} catch(e) {
			await this.write({}, true)
			return await this.read();
		}
	}

	async write(data, force = false) {
		if (!this.changed && !force)
			return

		if (typeof data === "undefined")
			data = this.data;

		await fs.writeFile(this.file, JSON.stringify(data), "utf8");

		this.changed = false
	}

	
	autosave(interval = 10000) {
		if (this.autosaveInterval)
			return

		this.autosaveInterval = setInterval(() => {
			this.write()
		}, interval);
	}

	clearAutosave() {
		if (this.autosaveInterval)
			clearInterval(this.autosaveInterval)
	}

	/**
	 * Retourne la valeur d'un élément via sa clé
	 * @param {string} key La clé de l'élément
	 * @param {any} def Si la clé n'existe pas, cette valeur sera retournée.
	 */
	get(key, def) {
		if (!this.data.hasOwnProperty(key))
			return def

		return this.data[key]
	}

	/**
	 * 
	 * @param {string} key La clé de l'élément à définir
	 * @param {any} value La valeur
	 */
	set(key, value) {
		this.changed = true

		this.data[key] = value
	}
}

module.exports = Settings;