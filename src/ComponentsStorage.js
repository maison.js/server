const fs = require("promisified-fs")
let instance = null

class ComponentsStorage {
	static init(file) {
		if (!instance)
			instance = new ComponentsStorage(file)

		return instance
	}

	constructor(file) {
		this.file = file
		this.data = []

		// Optimisation de l'IO disque
		this.changed = false

		this.autosaveInterval = null
	}

	async read() {
		try {
			await fs.access(this.file, fs.constants.F_OK);
			
			let content = await fs.readFile(this.file, "utf8");
			this.data = JSON.parse(content);
		} catch(e) {
			await this.write([], true)
			return await this.read();
		}
	}

	async write(data, force = false) {
		if (!this.changed && !force)
			return

		if (typeof data === "undefined")
			data = this.data;

		await fs.writeFile(this.file, JSON.stringify(data), "utf8");

		this.changed = false
	}

	
	autosave(interval = 10000) {
		if (this.autosaveInterval)
			return

		this.autosaveInterval = setInterval(() => {
			this.write()
		}, interval);
	}

	clearAutosave() {
		if (this.autosaveInterval)
			clearInterval(this.autosaveInterval)
    }

    getComponents() { return this.data }

    getComponent(i) { return this.data[i] }

    addComponent(component) {
        this.changed = true
        this.data.push(component)
        return this.data.length - 1
    }

    removeComponent(index) {
        if (this.data.length <= index)
            return

        this.changed = true
        this.data.splice(index, 1);
    }
}

module.exports = ComponentsStorage;