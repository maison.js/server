const EventEmitter = require('events')

module.exports = class Message {
    constructor(ws, message, crypting) {
        if (typeof message !== "string")
            throw new Error("Message argument in constructor should be a string.")

		this.ws = ws

		if (crypting)
			message = crypting.decrypt(message)

		this.message = JSON.parse(message)
		this.crypting = crypting
    }

    get id() {
        return this.message.id
    }

    get type() {
        return this.message.type
	}

	get v() { return this.message }

	getContent() { return this.message }

	_handleNewMessages(eventEmitter, newMessage) {
		let newMessage_ = new Message(this.ws, newMessage, this.crypting)

		if (newMessage_.type === this.type)
			eventEmitter.emit("type-answer", newMessage_)
		else
			eventEmitter.emit("answer", newMessage_)

		return eventEmitter
	}
	
	reply(obj, eventEmitter = false) {
		let data = JSON.stringify( Object.assign({
			"type": this.type,
			"id": this.id
		}, obj) )

		if (this.crypting)
			data = this.crypting.crypt(data)

		this.ws.send(data)

		if (!eventEmitter)
			return

		let e = new EventEmitter()
		this.ws.on('message', this._handleNewMessages.bind(this, e))
		return e
	}
}
