// permet l'async
try {
	require("./server")().catch(e => console.log(e))
} catch(e) {
	console.log(e)
}
process.on("uncaughtException", e => console.log('err', e))
process.on("unhandledRejection", e => console.log('err', e))