module.exports = async function() {
    try { require("fs").mkdirSync("user") } catch(e) {}
    
    const DEVELOPEMENT_MODE = process.platform === 'win32',
        /**
         * Logs for dev.
         */
        devlog = (...arguments) => DEVELOPEMENT_MODE ? console.log.apply(console, arguments) : null

    const WebSocket = require('ws'),
        Crypting = require('./src/Crypting'),
        Settings = require('./src/Settings'),
        Message = require('./src/Message'),

        WebsocketEndpoints = require('./websockets-endpoints'),


        wsServer = new WebSocket.Server({ port: 80, noServer: true }),
        settings = new Settings()

    WebsocketEndpoints.init({ DEVELOPEMENT_MODE, devlog, settings, wsServer })

    devlog('Lecture des paramètres.')
    await settings.read().then(() => devlog('Lecture terminée.'))

    if (DEVELOPEMENT_MODE)
        devlog('Mode de développement actif.')

    wsServer.on('listening', () => console.log("Serveur lancé!"))

    wsServer.on('connection', ws => {
        const crypting = new Crypting()
        devlog('Connexion en cours d\'un utilisateur...')

        ws.once('message', rawMessage => {
            try {
                let message = new Message(ws, rawMessage)

                if (message.type === 'init-dh') {
                    let content = message.getContent()

                    if (content.publicKey) {
                        ws.once('message', rawMessage => {

                            let message = new Message(ws, rawMessage, crypting)
            
                            if (message.type === 'hello') {
            
                                const isConfigured = settings.get("name", null) === null,
                                    passwordRequired = settings.get("password", false) !== false && settings.get("password", false) !== ""
            
                                if (!isConfigured)
                                    WebsocketEndpoints.setupPi(message)
                                else if (passwordRequired)
                                    WebsocketEndpoints.login(message)
                                else
                                    WebsocketEndpoints.connected(message)
                            } else
                                ws.terminate()
                        })

                        message.reply({ publicKey: crypting.getPublicKey() })
                        crypting.computeSecret(content.publicKey)

                        // maintenant, utilisateur et client ont une clé en commun
                    }
                }
            } catch(e) {
                ws.terminate()
            }
        })
    })

}