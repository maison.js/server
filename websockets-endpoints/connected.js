const ComponentsStorage = require("../src/ComponentsStorage"),
    componentsStorage = ComponentsStorage.init('./user/components.json')

Array.prototype.includesArray = function(arr) {
    for(let element of arr)
        if (!this.includes(element))
            return false

    return true
}

module.exports = function({ DEVELOPEMENT_MODE, devlog, settings, wsServer }) {
    componentsStorage.read()
    componentsStorage.autosave()

    const GPIOs = DEVELOPEMENT_MODE ? null : require('../src/Gpios')

    return function connectedEndpoint(message) {
        let eventListener = ee => {
            let evt = answerMessage => {
                // doooooooooo
                let content = answerMessage.getContent()

                if (answerMessage.type === 'set-pin-value')
                    if (!DEVELOPEMENT_MODE)
                        if (Object.keys(content).includesArray(["pinId", "value", "componentIndex"])) {
                            try {
                                GPIOs.setup(content.pinId, { mode: GPIOs.OUTPUT })
                                    .set(content.value)

                                if (content.componentIndex !== null)
                                    componentsStorage.data[content.componentIndex]["state"] = content.value
                                return answerMessage.reply({ "result": 0 })
                            } catch(e) {
                                if (DEVELOPEMENT_MODE)
                                    return answerMessage.reply({ "result": 0 })
                             
                                return answerMessage.reply({ "result": -1 })
                            }
                        }
                    else
                        return answerMessage.reply({ "result": 0 })

                if (answerMessage.type === 'get-components')
                    answerMessage.reply({ "components": componentsStorage.getComponents() })

                if (answerMessage.type === 'add-component')
                    answerMessage.reply({ "result": componentsStorage.addComponent(content.component) })

                if (answerMessage.type === 'remove-component')
                    answerMessage.reply({ "result": componentsStorage.removeComponent(content.componentIndex) })

                if (answerMessage.type === 'disconnect')
                    ee.off('answer', evt)
            }

            return evt
        }

        let ee = message.reply({
            "name": settings.get("name", "aucun"),
            "configured": true,
            "passwordRequired": false
        }, true)
        
        ee.on('answer', eventListener(ee))
    }
}