let instance = null;

(function createInstance() {
    if (!instance)
        return module.exports.init = function(instanceObjects) {
            instance = this

            this.init = function() {}

            require('fs').readdirSync(__dirname)
                .filter(el => el !== 'index.js' && el.indexOf(".") > -1)
                .forEach(v => this[require('path').basename(v, ".js")] = require('./' + v)(instanceObjects) )
        }

    module.exports = instance
})()