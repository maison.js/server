const ConnectedEndpoint = require('./connected'),
    sha256 = require("js-sha256")

module.exports = (lol) => {
    const connectedEndpoint = ConnectedEndpoint(lol),
        { devlog, settings } = lol

    return function loginEndpoint(message) {

        let evtListener = ee => {
            let evt = answerMessage => {
                if (answerMessage.type === 'login')
                    if (answerMessage.getContent().password !== settings.get("password"))
                        return answerMessage.reply({ result: false })
                    else {
                        ee.off('answer', evt)
                        connectedEndpoint(answerMessage.reply({ result: true }))
                    }
            }

            return evt
        }

        let ee = message.reply({
            "name": settings.get("name", "aucun"),
            "configured": true,
            "passwordRequired": true
        }, true)
        
        ee.on('answer', evtListener(ee))
    }
}